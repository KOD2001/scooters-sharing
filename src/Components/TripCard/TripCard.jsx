import React from 'react';
import Card from '@mui/material/Card';
import './TripCard.css';

export const TripCard = ({tripId, scooterId, startDate, endDate, cost}) => {
    return (
        <Card className="card">
            <h2 className="card__tripId">ID поездки {tripId}</h2>
            <p className="card__scooterId">ID самоката {scooterId}</p>
            <p className="card__startDate">Дата начала {startDate}</p>
            <p className="card__endDate">Дата окончания {endDate}</p>
            <p className="card__cost">Стоимость <b>{cost}</b></p>
        </Card>
    );
};
