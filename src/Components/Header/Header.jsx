import { useEffect, useState } from 'react';
import { Tabs, Tab, Button } from '@mui/material';
import { useNavigate } from 'react-router-dom';
import './Header.css';

export const Header = () => {
    const [value, setValue] = useState('map');
    const navigate = useNavigate();

    const handleChange = (event, newValue) => setValue(newValue);

    useEffect(() => {
        navigate(`/${value}`);
    }, [value, navigate])

    return (
        <div className="header">
            <img src={require('../../Icons/scooter.png')} alt="Logo" />
            <Tabs
                value={value}
                onChange={handleChange}
                textColor="secondary"
                indicatorColor="secondary"
                aria-label="secondary tabs example">
                <Tab value="map" label="Карта" />
                <Tab value="history" label="История" />
            </Tabs>
            <Button size="large" color="secondary">Выйти</Button>
        </div>
    );
};
