import { Button, Card } from '@mui/material';
import { useDispatch } from 'react-redux';
import { useState } from 'react';
import { endRideAction, endRideThunk } from '../../Redux';
import { useSelector } from 'react-redux';
import moment from 'moment/moment';
import Timer from 'react-compound-timer/build';
import './OnRide.css';

export const OnRide = () => {
    const [minutes, setMinutes] = useState(0);
    const [seconds, setSeconds] = useState(0);
    const [isFinishing, setIsFinishing] = useState(false);
    const {tripId, scooterId, startDate, endDate, cost} = useSelector(state => state.ride);
    const dispatch = useDispatch();

    const calcCost = (seconds, minutes) => {
        const unit = 0.17;
        return Math.round((seconds * unit) + (minutes * 60 * 0.17));
    }

    const endRideClickHandler = () => {
        const cost = calcCost(seconds, minutes);
        dispatch(endRideThunk({tripId, cost}));
        setIsFinishing(true);
    }

    return (
        <div className="onRide">
            <Card className="onRide__card">
                <div className="onRide__header">
                    <p className="onRide__tripId">Номер поездки: {tripId}</p>
                    <p className="onRide__scooterId">Номер самоката: {scooterId}</p>
                </div>
                <p className="onRide__startDate">Время начала поездки: {moment(startDate, 'LLL').format('LT')}</p>
                <Timer>
                    {({stop}) => {
                        isFinishing && stop();
                        return (
                            <div className='onRide__timerCounter'>
                                <Timer.Minutes 
                                    formatValue={(value) => {
                                        setMinutes(value);
                                        return `${(value < 10 ? `0${value}` : value)}`
                                    }} 
                                />
                                :
                                <Timer.Seconds 
                                    formatValue={(value) => {
                                        setSeconds(value);
                                        return `${(value < 10 ? `0${value}` : value)}`
                                    }} 
                                />
                            </div>
                        )
                    }}
                </Timer>
                {
                    isFinishing &&
                    <>
                        <p className="onRide__date">Время окончания поездки: {moment(endDate, 'LLL').format('LT')}</p>
                        <p className="onRide__cost">Стоимость поездки: <b>{cost}</b></p>
                    </>
                }
                {
                    isFinishing
                        ?   <Button
                                onClick={() => {
                                    dispatch(endRideAction());
                                    setIsFinishing(false);
                                }}
                                size="large" 
                                color="secondary">
                                На карту
                            </Button>
                        :   <Button
                                onClick={endRideClickHandler}
                                size="large" 
                                color="secondary">
                                Завершить поездку
                            </Button>

                }
            </Card>
        </div>
    );
};
