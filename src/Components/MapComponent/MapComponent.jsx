import { YMaps, Map, Placemark } from 'react-yandex-maps';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchScooters, startRideThunk } from '../../Redux';
import { OnRide } from '../OnRide';
import { History } from '../History';
import { useLocation } from 'react-router-dom';
import './MapComponent.css';

export const MapComponent = () => {
    const location = useLocation();
    const dispatch = useDispatch();
    const scooters = useSelector(state => state.scooters.scooters);
    const {isStarted} = useSelector(state => state.ride);
    const placemarkProps = {
        defaultProperties:{
            hintContent: "<h2>Самокат</h2>",
            balloonContent: `
            <div class="scooter-card">
                <h2>Арендовать самокат</h2>
                <p>10р./мин.</p>
                <button class="start-ride">Начать поездку</button>
            </div>
            `,
        }, 
        defaultOptions:{ 
            iconLayout: 'default#image', 
            iconImageHref: require("../../Icons/scooter-pin.png"),  
            iconImageSize: [26, 26],
            hideIconOnBalloonOpen: false,
            balloonOffset: [3, -40]
        }
    }

    useEffect(() => {
        dispatch(fetchScooters());
    }, [dispatch]);

    const startRideClickHandler = (selectedScooter) => {
        setTimeout(() => {
            const startRideButton = document.querySelector('.start-ride');
            if (startRideButton) {
                startRideButton.addEventListener('click', () => {
                    dispatch(startRideThunk(selectedScooter));
                });
            }
        }, 0);
    }

    return (
        <>
            <div className={location.pathname === '/history' ? 'hidden' : 'map'}>
                {isStarted 
                    ? <OnRide /> 
                    : <YMaps>
                            <Map
                                style={{width: '100%', height: '100%'}} 
                                defaultState={{ center: [59.931, 30.327], zoom: 11 }}>
                                {
                                    scooters.map(item =>
                                        <Placemark
                                            onClick={() => {
                                                startRideClickHandler(item.id);
                                            }}
                                            key={item.id}
                                            modules={['geoObject.addon.balloon', 'geoObject.addon.hint']} 
                                            defaultGeometry={item.data.geometry}
                                            {...placemarkProps}
                                        />
                                    )
                                }
                            </Map>
                        </YMaps>
                }
            </div>
            <History />
        </>
    );
};
