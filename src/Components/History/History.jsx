import { useDispatch, useSelector } from 'react-redux';
import { fetchTrips } from '../../Redux';
import { useEffect } from 'react';
import { TripCard } from '../TripCard';
import { useLocation } from 'react-router-dom';
import './History.css';

export const History = () => {
    const dispatch = useDispatch();
    const trips = useSelector(state => state.trips.trips);
    const location = useLocation();

    useEffect(() => {
        if (location.pathname === '/history') {
            dispatch(fetchTrips());
        }
    }, [dispatch, location]);

    return (
        <div className={location.pathname === '/history' ? 'history' : 'hidden'}>
            <div className="history__list">
                {
                    trips &&
                    trips.map(item => 
                        <TripCard
                            key={item.id} 
                            tripId={item.id} 
                            scooterId={item.data.scooterId} 
                            startDate={item.data.startDate} 
                            endDate={item.data.endDate}
                            cost={item.data.cost} 
                        />
                    )
                }
            </div>
        </div>
    );
};
