import { collection, getDocs, addDoc, updateDoc, doc } from 'firebase/firestore/lite';
import { db } from '../db';

export const getScooters = async () => {
    const scootersCol = collection(db, 'scooters');
    const scootersSnapshot = await getDocs(scootersCol);
    const scootersList = scootersSnapshot.docs.map(doc => {
        return {
            data: doc.data(), 
            id: doc.id
        };
    });
    return scootersList;
}

export const getAllTrips = async () => {
    const tripsCol = collection(db, 'trips');
    const tripsSnapshot = await getDocs(tripsCol);
    const tripsList = tripsSnapshot.docs.map(doc => {
        return {
            data: doc.data(), 
            id: doc.id
        };
    });
    return tripsList;
}

export const startRide = async (scooterId, startDate) => {
    const docRef = await addDoc(collection(db, 'trips'), {
        scooterId: scooterId,
        startDate: startDate,
        endDate: '',
        cost: 0
    });

    const data = {
        tripId: docRef.id,
        scooterId: scooterId,
        startDate: startDate
    }

    return data;
}

export const endRide = async (endDate, cost, tripId) => {
    const tripRef = doc(db, 'trips', tripId);
    await updateDoc(tripRef, {
        endDate: endDate,
        cost: cost
    });
}