// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore/lite';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyCPDk-PizTHN4woNwiT089vxv85dAcfBvg",
  authDomain: "scooters-702bd.firebaseapp.com",
  projectId: "scooters-702bd",
  storageBucket: "scooters-702bd.appspot.com",
  messagingSenderId: "1001309857797",
  appId: "1:1001309857797:web:8c62e434aa4b8c6bb0146c",
  measurementId: "G-K5LRV65NBV"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);