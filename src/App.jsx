import { Header, MapComponent } from './Components';
import { Routes, Route } from 'react-router-dom';
import './App.css';

function App() {
	return (
		<div className="App">
			<Header />
			<Routes>
				<Route path="/map" element={<MapComponent />} />
				<Route path="/history" element={<MapComponent />} />
			</Routes>
		</div>
	);
}

export default App;
