import { createAsyncThunk } from '@reduxjs/toolkit';
import { getScooters, getAllTrips, startRide, endRide } from '../../Apis';
import moment from 'moment/moment';
import 'moment/locale/ru';

export const fetchScooters = createAsyncThunk(
    'scooters/fetchScooters',
    async () => {
        const response = getScooters().then(res => res);
        return response;
    }
);

export const fetchTrips = createAsyncThunk(
    'trips/fetchTrips',
    async () => {
        const response = getAllTrips().then(res => res);
        return response;
    }
);

export const startRideThunk = createAsyncThunk(
    'ride/startRide',
    async (selectedScooter) => {
        const currentDate = moment().locale('ru').format('LLL');
        const data = startRide(selectedScooter, currentDate);

        return data;
    }
);

export const endRideThunk = createAsyncThunk(
    'ride/endRide',
    async ({tripId, cost}) => {
        const currentDate = moment().locale('ru').format('LLL');
        endRide(currentDate, cost, tripId);
        const data = {
            endDate: currentDate,
            cost: cost
        }

        return data;
    }
);
