import { createSlice } from '@reduxjs/toolkit';
import { fetchScooters } from '../Thunks';

export const scootersSlice = createSlice({
	name: 'scooters',
	initialState: {
		scooters: [],
		error: ''
	},
	reducers: {},
	extraReducers: {
		[fetchScooters.fulfilled]: (state, action) => {
			state.scooters = action.payload;
			state.error = '';
		},
		[fetchScooters.rejected]: (state) => {
			state.error = 'Не удалось загрузить самокаты';
		},
	}
});

export default scootersSlice.reducer;
