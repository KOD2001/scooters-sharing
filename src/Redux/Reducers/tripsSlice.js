import { createSlice } from '@reduxjs/toolkit';
import { fetchTrips } from '../Thunks';

export const tripsSlice = createSlice({
	name: 'trips',
	initialState: {
		trips: [],
		error: ''
	},
	reducers: {},
	extraReducers: {
		[fetchTrips.fulfilled]: (state, action) => {
			state.trips = action.payload;
			state.error = '';
		},
		[fetchTrips.rejected]: (state) => {
			state.error = 'Не удалось загрузить поездки';
		},
	}
});

export default tripsSlice.reducer;
