import { createSlice } from '@reduxjs/toolkit';
import { startRideThunk, endRideThunk } from '../Thunks';

export const rideSlice = createSlice({
	name: 'ride',
	initialState: {
        isStarted: false,
        scooterId: '',
		tripId: '',
		startDate: '',
		endDate: '',
		cost: 0
	},
	reducers: {
		endRideAction(state) {
			state.isStarted = !state.isStarted;
			state.scooterId = '';
			state.tripId = '';
			state.startDate = '';
			state.endDate = '';
			state.cost = 0;
		}
	},
    extraReducers: {
		[startRideThunk.fulfilled]: (state, action) => {
            state.isStarted = !state.isStarted;
			state.scooterId = action.payload.scooterId;
			state.tripId = action.payload.tripId;
			state.startDate= action.payload.startDate;
		},
		[endRideThunk.fulfilled]: (state, action) => {
			state.endDate = action.payload.endDate;
			state.cost = action.payload.cost;
		},
    }
});

export const {endRideAction} = rideSlice.actions;

export default rideSlice.reducer;
