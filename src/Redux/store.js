import { configureStore, compose } from '@reduxjs/toolkit';
import scootersReducer from './Reducers/scooterSlice';
import tripsReducer from './Reducers/tripsSlice';
import rideReducer from './Reducers/rideSlice';


export default configureStore({
  reducer: {
    scooters: scootersReducer,
    trips: tripsReducer,
    ride: rideReducer
  },
  devTools: window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
});